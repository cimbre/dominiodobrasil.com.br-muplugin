<?php
/*
Plugin Name: Dominio do Brasil - MU-Plugin
Plugin URI: https://dominiodobrasil.com.br
Description: Customizações para o site dominiodobrasil.com.br
Author: Estúdio Cimbre 
Version: 1.0.0.5
Author URI: http://estudiocimbre.com.br
Text Domain: dominiodobrasil
*/

/**
 * Load Translations
 */
function dominiodobrasil_load_textdomain()     
{
    load_muplugin_textdomain('dominiodobrasil', basename(dirname(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'dominiodobrasil_load_textdomain');
//******************************************************************************************************************************************************/

/**
 * Home Custom Fields
 */
function cmb_dominiodobrasil_home() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_dominiodobrasil_home_';
    
    /**
     * Initiate the metabox
     */

    $cmb = new_cmb2_box(
        array(
            'id'            => 'dominio_home_id',
            'title'         => __('Dominio do Brasil', 'dominiodobrasil'),
            'object_types'  => array('page'), // post type
            //'show_on'       =>  array( 'key' => 'front-page', 'value' => '' ),
            'show_on_cb' => 'dominiodobrasil_show_on_page_home', // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );
    
    // Hero Title
    $cmb->add_field( 
        array(
            'name'       => __('Hero Title', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'hero-title',
            'type'       => 'text',
        )
    );

    // Hero Description
    $cmb->add_field( 
        array(
            'name'       => __('Hero Description', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'hero-description',
            'type'       => 'textarea_small',
        )
    );

    // Phone
    $cmb->add_field( 
        array(
            'name'       => __('Phone', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'phone',
            'type'       => 'text',
        )
    );

    // Address
    $cmb->add_field( 
        array(
            'name'       => __('Address', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'address',
            'type'       => 'text',
        )
    );

    // Map
    $cmb->add_field( 
        array(
            'name'       => __('Map', 'dominiodobrasil'),
            'desc'       => __('IFRAME html code', 'dominiodobrasil'),
            'id'         => $prefix . 'map',
            'type'       => 'textarea_code',
        )
    );

    // Footer Description
    $cmb->add_field( 
        array(
            'name'       => __('Footer Description', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'footer-description',
            'type'       => 'textarea_small',
        )
    );

    // Facebook Profile
    $cmb->add_field( 
        array(
            'name'       => __('Facebook Profile (URL)', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'facebook-profile',
            'type'       => 'text_url',
        )
    );
}
function dominiodobrasil_show_on_page_home($cmb) 
{
    $slug = get_page_template_slug($cmb->object_id);
    return in_array($slug, array('views/front-page.blade.php','front-page', 'home'));
}
add_action('cmb2_admin_init', 'cmb_dominiodobrasil_home');

/**
 * About Custom Fields
 */
function cmb_dominiodobrasil_about() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_dominiodobrasil_about_';
    
    /**
    * Initiate the metabox
    */

    $cmb = new_cmb2_box(
        array(
            'id'            => 'dominio_about_id',
            'title'         => __('Structure', 'dominiodobrasil'),
            'object_types'  => array('page'), // post type
            'show_on_cb' => 'dominiodobrasil_show_on_page_about', // function should return a bool value
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Structure 
    $cmb->add_field( 
        array(
            'name'    => __('Structure', 'dominiodobrasil'),
            'desc'    => 'field description (optional)',
            'id'      => 'wiki_test_wysiwyg',
            'type'    => 'wysiwyg',
            'options' => array(),
        )
    );

    // Gallery fields
    $cmb->add_field( 
        array(
            'name' => __('Gallery', 'dominiodobrasil'),
            'id'   => $prefix . 'gallery',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'dominiodobrasil'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'dominiodobrasil'), // default: "Remove Image"
                'file_text' => __('File', 'dominiodobrasil'), // default: "File:"
                'file_download_text' => __('Download', 'dominiodobrasil'), // default: "Download"
                'remove_text' => __('Remove', 'dominiodobrasil'),
                'use_text' => __('Remove', 'dominiodobrasil'), // default: "Remove"
                'upload_file'  => __('Use this file', 'dominiodobrasil'),
                'upload_files' => __('Use these files', 'dominiodobrasil'),
                'remove_image' => __('Remove Image', 'dominiodobrasil'),
                'remove_file'  => __('Remove', 'dominiodobrasil'),
                'file'         => __('File:', 'dominiodobrasil'),
                'download'     => __('Download', 'dominiodobrasil'),
                'check_toggle' => __('Select / Deselect All', 'dominiodobrasil'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );
}

function dominiodobrasil_show_on_page_about($cmb) 
{
    $slug = get_page_template_slug($cmb->object_id);
    return in_array($slug, array('views/template-about.blade.php'));
}
add_action('cmb2_admin_init', 'cmb_dominiodobrasil_about');

/********************************************************************************************************************************
 * Register post types
 */

/**
 * Solutions
 */
//if (!post_type_exists('dominio_solutions')) {
function cpt_dominiodobrasil_solutions() 
{
    $labels = array(
            'name'                  => __('Solutions', 'dominiodobrasil'),
            'singular_name'         => __('Solution', 'dominiodobrasil'),
            'menu_name'             => __('Solutions', 'dominiodobrasil'),
            'name_admin_bar'        => __('Solution', 'dominiodobrasil'),
            'archives'              => __('Solution Archives', 'dominiodobrasil'),
            'attributes'            => __('Solution Attributes', 'dominiodobrasil'),
            'parent_item_colon'     => __('Parent Solution:', 'dominiodobrasil'),
            'all_items'             => __('All Solutions', 'dominiodobrasil'),
            'add_new_item'          => __('Add New Solution', 'dominiodobrasil'),
            'add_new'               => __('Add new _solution', 'dominiodobrasil'),
            'new_item'              => __('New Solution', 'dominiodobrasil'),
            'edit_item'             => __('Edit Solution', 'dominiodobrasil'),
            'update_item'           => __('Update Solution', 'dominiodobrasil'),
            'view_item'             => __('View Solution', 'dominiodobrasil'),
            'view_items'            => __('View Solutions', 'dominiodobrasil'),
            'search_items'          => __('Search Solution', 'dominiodobrasil'),
            'not_found'             => __('Not found', 'dominiodobrasil'),
            'not_found_in_trash'    => __('Not found in Trash', 'dominiodobrasil'),
            'featured_image'        => __('Featured Image', 'dominiodobrasil'),
            'set_featured_image'    => __('Set featured image', 'dominiodobrasil'),
            'remove_featured_image' => __('Remove featured image', 'dominiodobrasil'),
            'use_featured_image'    => __('Use as featured image', 'dominiodobrasil'),
            'insert_into_item'      => __('Insert into solution', 'dominiodobrasil'),
            'uploaded_to_this_item' => __('Uploaded to this solution', 'dominiodobrasil'),
            'items_list'            => __('Solutions list', 'dominiodobrasil'),
            'items_list_navigation' => __('Solutions list navigation', 'dominiodobrasil'),
            'filter_items_list'     => __('Filter Solutions list', 'dominiodobrasil'),
    );
    $rewrite = array(
            'slug'                  => __('solutions', 'dominiodobrasil'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Solution', 'dominiodobrasil'),
            'description'           => __('Dominio do Brasil Solutions', 'dominiodobrasil'),
            'labels'                => $labels,
            'supports'              => array('title', 'thumbnail', 'editor', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="black" d="M501.801 56.448H10.199C4.566 56.448 0 61.015 0 66.648V202.41c0 2.806 1.157 5.488 3.196 7.415l48.307 45.623C53.879 366.192 144.696 455.551 256 455.551s202.121-89.36 204.497-200.104l48.307-45.623a10.2 10.2 0 0 0 3.196-7.415V66.648c0-5.633-4.566-10.2-10.199-10.2zM256 435.153c-98.122 0-178.567-77.137-183.876-173.957h367.752c-5.309 96.82-85.754 173.957-183.876 173.957zm235.602-237.14L446.3 240.799H65.7l-31.362-29.62H348.89c5.633 0 10.199-4.566 10.199-10.199s-4.566-10.199-10.199-10.199H20.398V76.847h471.203v121.166z"/><path d="M430.347 190.781H410.34c-5.633 0-10.199 4.566-10.199 10.199s4.566 10.199 10.199 10.199h20.007c5.633 0 10.199-4.566 10.199-10.199s-4.566-10.199-10.199-10.199zM256 348.362c-33.515 0-58.788 15.444-58.788 35.923s25.274 35.923 58.788 35.923 58.788-15.444 58.788-35.923c0-20.48-25.273-35.923-58.788-35.923zm0 51.447c-23.078 0-38.39-9.344-38.39-15.524S232.922 368.76 256 368.76s38.39 9.344 38.39 15.524-15.312 15.525-38.39 15.525z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('dominio_solutions', $args); 
} 
add_action('init', 'cpt_dominiodobrasil_solutions', 0);

/**
 * Solutions Custom Fields
 */
function cmb_dominiodobrasil_solutions() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_dominiodobrasil_solution_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'dominio_solutions',
            'title'         => __('Solution', 'dominiodobrasil'),
            'object_types'  => array('dominio_solutions'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    // Home Description    $cmb->add_field( 
    $cmb->add_field( 
        array(
            'name'       => __('Home Description', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'home-description',
            'type'       => 'textarea',
            //'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
            // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
            // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
            // 'on_front'        => false, // Optionally designate a field to wp-admin only
            // 'repeatable'      => true,
        )
    );

    // Gallery fields
    $cmb->add_field( 
        array(
            'name' => __('Gallery', 'dominiodobrasil'),
            'id'   => $prefix . 'gallery',
            'type' => 'file_list',
            // Optional:
            'options' => array(
                    'url' => false, // Hide the text input for the url
            ),
            'text' => array(
                'add_upload_files_text' => __('Add Images', 'dominiodobrasil'), // default: "Add or Upload Files"
                'remove_image_text' => __('Remove Image', 'dominiodobrasil'), // default: "Remove Image"
                'file_text' => __('File', 'dominiodobrasil'), // default: "File:"
                'file_download_text' => __('Download', 'dominiodobrasil'), // default: "Download"
                'remove_text' => __('Remove', 'dominiodobrasil'),
                'use_text' => __('Remove', 'dominiodobrasil'), // default: "Remove"
                'upload_file'  => __('Use this file', 'dominiodobrasil'),
                'upload_files' => __('Use these files', 'dominiodobrasil'),
                'remove_image' => __('Remove Image', 'dominiodobrasil'),
                'remove_file'  => __('Remove', 'dominiodobrasil'),
                'file'         => __('File:', 'dominiodobrasil'),
                'download'     => __('Download', 'dominiodobrasil'),
                'check_toggle' => __('Select / Deselect All', 'dominiodobrasil'),
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                    //'type' => 'image/jpeg', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    'type' => array(
                    // 	'image/gif',
                            'image/jpeg',
                            'image/png',
                    ),
            ),
            'preview_size' => array(160, 90) //'large', // Image size to use when previewing in the admin.
        )
    );
}
add_action('cmb2_admin_init', 'cmb_dominiodobrasil_solutions');
//}

/**
 * Diferentials
 */
//if (!post_type_exists('dominio_diferentials')) {
function cpt_dominiodobrasil_diferentials() 
{
    $labels = array(
            'name'                  => __('Diferentials', 'dominiodobrasil'),
            'singular_name'         => __('Diferential', 'dominiodobrasil'),
            'menu_name'             => __('Diferentials', 'dominiodobrasil'),
            'name_admin_bar'        => __('Diferential', 'dominiodobrasil'),
            'archives'              => __('Diferential Archives', 'dominiodobrasil'),
            'attributes'            => __('Diferential Attributes', 'dominiodobrasil'),
            'parent_item_colon'     => __('Parent Diferential:', 'dominiodobrasil'),
            'all_items'             => __('All Diferentials', 'dominiodobrasil'),
            'add_new_item'          => __('Add New Diferential', 'dominiodobrasil'),
            'add_new'               => __('Add diferential', 'dominiodobrasil'),
            'new_item'              => __('New Diferential', 'dominiodobrasil'),
            'edit_item'             => __('Edit Diferential', 'dominiodobrasil'),
            'update_item'           => __('Update Diferential', 'dominiodobrasil'),
            'view_item'             => __('View Diferential', 'dominiodobrasil'),
            'view_items'            => __('View Diferentials', 'dominiodobrasil'),
            'search_items'          => __('Search Diferential', 'dominiodobrasil'),
            'not_found'             => __('Not found', 'dominiodobrasil'),
            'not_found_in_trash'    => __('Not found in Trash', 'dominiodobrasil'),
            'featured_image'        => __('Featured Image', 'dominiodobrasil'),
            'set_featured_image'    => __('Set featured image', 'dominiodobrasil'),
            'remove_featured_image' => __('Remove featured image', 'dominiodobrasil'),
            'use_featured_image'    => __('Use as featured image', 'dominiodobrasil'),
            'insert_into_item'      => __('Insert into diferential', 'dominiodobrasil'),
            'uploaded_to_this_item' => __('Uploaded to this diferential', 'dominiodobrasil'),
            'items_list'            => __('Diferentials list', 'dominiodobrasil'),
            'items_list_navigation' => __('Diferentials list navigation', 'dominiodobrasil'),
            'filter_items_list'     => __('Filter Diferentials list', 'dominiodobrasil'),
    );
    $rewrite = array(
            'slug'                  => __('diferentials', 'dominiodobrasil'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Diferential', 'dominiodobrasil'),
            'description'           => __('Dominio do Brasil Diferentials', 'dominiodobrasil'),
            'labels'                => $labels,
            'supports'              => array('title', 'editor', 'thumbnail', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 6,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 43.16 48"><g fill="black"><path d="M43.15 24v10.69a2.37 2.37 0 0 1-1.3 2.24l-7 3.92-12.1 6.83a2.25 2.25 0 0 1-2.32 0L2.25 37.45c-.36-.2-.72-.39-1.07-.61a2.27 2.27 0 0 1-1.17-2Q0 24 0 13.18a2.32 2.32 0 0 1 1.3-2.11l9.86-5.54C14.26 3.8 17.35 2 20.44.32a2.23 2.23 0 0 1 2.28 0l19 10.68a2.55 2.55 0 0 1 1.44 2.44c-.03 3.51-.01 7.04-.01 10.56zm-4.74 0v-8.93a.72.72 0 0 0-.43-.73L21.84 5.28a.6.6 0 0 0-.5 0Q13.16 9.86 5 14.48a.55.55 0 0 0-.23.4v18.21a.59.59 0 0 0 .25.43l16.31 9.2a.49.49 0 0 0 .55 0l16.23-9.13a.54.54 0 0 0 .32-.55C38.4 30 38.41 27 38.41 24z"/><path d="M33.39 24h-6.58c-.37 0-.47.12-.47.48v8.68c0 .35-.08.45-.44.45h-9.1V24h-7l11.81-11.8z"/></g></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('dominio_diferentials', $args); 
} 
add_action('init', 'cpt_dominiodobrasil_diferentials', 0);

/**
 * Diferentials Custom Fields
 */
function cmb_dominiodobrasil_diferentials() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_dominiodobrasil_diferentials_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'dominio_diferentials',
            'title'         => __('Diferential', 'dominiodobrasil'),
            'object_types'  => array('dominio_diferentials'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Subtitle
    $cmb->add_field( 
        array(
            'name'       => __('Subtitle', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'subtitle',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_dominiodobrasil_diferentials');
//} 

/**
 * Testimonials
 */
//if (!post_type_exists('dominio_testimonials')) {
function cpt_dominiodobrasil_testimonials() 
{
    $labels = array(
            'name'                  => __('Testimonials', 'dominiodobrasil'),
            'singular_name'         => __('Testimonial', 'dominiodobrasil'),
            'menu_name'             => __('Testimonials', 'dominiodobrasil'),
            'name_admin_bar'        => __('Testimonial', 'dominiodobrasil'),
            'archives'              => __('Testimonial Archives', 'dominiodobrasil'),
            'attributes'            => __('Testimonial Attributes', 'dominiodobrasil'),
            'parent_item_colon'     => __('Parent Testimonial:', 'dominiodobrasil'),
            'all_items'             => __('All Testimonials', 'dominiodobrasil'),
            'add_new_item'          => __('Add New Testimonial', 'dominiodobrasil'),
            'add_new'               => __('Add testimonial', 'dominiodobrasil'),
            'new_item'              => __('New Testimonial', 'dominiodobrasil'),
            'edit_item'             => __('Edit Testimonial', 'dominiodobrasil'),
            'update_item'           => __('Update Testimonial', 'dominiodobrasil'),
            'view_item'             => __('View Testimonial', 'dominiodobrasil'),
            'view_items'            => __('View Testimonials', 'dominiodobrasil'),
            'search_items'          => __('Search Testimonial', 'dominiodobrasil'),
            'not_found'             => __('Not found', 'dominiodobrasil'),
            'not_found_in_trash'    => __('Not found in Trash', 'dominiodobrasil'),
            'featured_image'        => __('Featured Image', 'dominiodobrasil'),
            'set_featured_image'    => __('Set featured image', 'dominiodobrasil'),
            'remove_featured_image' => __('Remove featured image', 'dominiodobrasil'),
            'use_featured_image'    => __('Use as featured image', 'dominiodobrasil'),
            'insert_into_item'      => __('Insert into testimonial', 'dominiodobrasil'),
            'uploaded_to_this_item' => __('Uploaded to this testimonial', 'dominiodobrasil'),
            'items_list'            => __('Testimonials list', 'dominiodobrasil'),
            'items_list_navigation' => __('Testimonials list navigation', 'dominiodobrasil'),
            'filter_items_list'     => __('Filter Testimonials list', 'dominiodobrasil'),
    );
    $rewrite = array(
            'slug'                  => __('testimonials', 'dominiodobrasil'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Testimonial', 'dominiodobrasil'),
            'description'           => __('Dominio do Brasil Testimonials', 'dominiodobrasil'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 7,
            'menu_icon'             => 'data:image/svg+xml;base64,'. base64_encode('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="black" d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/></svg>'),
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => false,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('dominio_testimonials', $args); 
} 
add_action('init', 'cpt_dominiodobrasil_testimonials', 0);

/**
 * Testimonials Custom Fields
 */
function cmb_dominiodobrasil_testimonials() 
{
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_dominiodobrasil_testimonial_';
    
    /**
    * Initiate the metabox
    */
    $cmb = new_cmb2_box(
        array(
            'id'            => 'dominio_testimonials',
            'title'         => __('Testimonial', 'dominiodobrasil'),
            'object_types'  => array('dominio_testimonials'), // Post type
            'context'       => 'normal',
            'priority'      => 'high',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
        )
    );

    //Photo
    $cmb->add_field(
        array(
        'name'        => __('Photo', 'dominiodobrasil'),
        'description' => '',
        'id'          => $prefix . 'photo',
        'type'        => 'file',
        // Optional:
        'options' => array(
                'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
                'add_upload_file_text' => __('Add Image', 'dominiodobrasil'), // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
                //'type' => 'image/jpeg', // Make library only display PDFs.
                // Or only allow gif, jpg, or png images
                'type' => array(
                // 	'image/gif',
                        'image/jpeg',
                        'image/png',
            ),
        ),
        'preview_size' => array(180, 180) //'large', // Image size to use when previewing in the admin.
        )
    );

    //Title
    $cmb->add_field( 
        array(
            'name'       => __('Title', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'title',
            'type'       => 'text',
        )
    );

    //Description
    $cmb->add_field( 
        array(
            'name'       => __('Description', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'description',
            'type'       => 'textarea',
        )
    );

    //Name
    $cmb->add_field( 
        array('name'       => __('Name', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'name',
            'type'       => 'text',
        )
    );
    
    //Local
    $cmb->add_field( 
        array(
            'name'       => __('Local', 'dominiodobrasil'),
            'desc'       => '',
            'id'         => $prefix . 'local',
            'type'       => 'text',
        )
    );
}
add_action('cmb2_admin_init', 'cmb_dominiodobrasil_testimonials');
//} 
